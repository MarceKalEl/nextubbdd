package com.navegacioncajon.navegacioncajon;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Marce on 24/7/2016.
 */
public class Organizacion extends Fragment {
    private EditText Idorg,Nombreorg, Emailorg,Telefonorg;

    @Override
    //metodo para crear la vista del layout del fragmento
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.organizacion_layout,container,false);
        Button aboton = (Button)rootView.findViewById(R.id.ORGADD);
        Button cboton = (Button)rootView.findViewById(R.id.ORGCONS);
        Button dboton = (Button)rootView.findViewById(R.id.ORGDEL);
        Button mboton = (Button)rootView.findViewById(R.id.ORGMOD);
        aboton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Idorg=(EditText)rootView.findViewById(R.id.ORGTEXT);
                Nombreorg=(EditText)rootView.findViewById(R.id.NOMORGTEXT);
                Emailorg=(EditText)rootView.findViewById(R.id.EMORGTEXT);
                Telefonorg=(EditText)rootView.findViewById(R.id.FONOTEXT);


                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),"administracion",null,1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Idorg.getText().toString();
                String nombre = Nombreorg.getText().toString();
                String email = Emailorg.getText().toString();
                String telefono = Telefonorg.getText().toString();

                ContentValues registrar = new ContentValues();
                registrar.put("idorgs",id);
                registrar.put("nombreorgs",nombre);
                registrar.put("emailorgs",email);
                registrar.put("telefonorgs",telefono);
                database.insert("orgs",null,registrar); //registramos en la BBDD
                database.close();//cerramos la conexion
                // Limpiamos Los Campos
                Idorg.setText("");
                Nombreorg.setText("");
                Emailorg.setText("");
                Telefonorg.setText("");
                Toast.makeText(getActivity(),"Se Registraton Los datos correctamente",Toast.LENGTH_LONG).show();
            }
        });

        cboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Idorg.getText().toString();
                Cursor fila = database.rawQuery(
                        "select nombreorgs,emailorgs,telefonorgs from orgs where idorgs=" + id
                                + "", null);
                if (fila.moveToFirst()) {
                    Nombreorg.setText(fila.getString(0));
                    Emailorg.setText(fila.getString(1));
                    Telefonorg.setText(fila.getString(2));
                } else
                    Toast.makeText(getActivity(), "No existe una persona con este Id",
                            Toast.LENGTH_SHORT).show();
                database.close();


            }
        });

        dboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Idorg.getText().toString();
                int eliminar = database.delete("orgs", "idorgs=" + id + "", null);
                database.close();
                Idorg.setText("");
                Nombreorg.setText("");
                Emailorg.setText("");
                Telefonorg.setText("");
                if (eliminar == 1)
                    Toast.makeText(getActivity(), "Se Elimino la Organización Correctamente",
                            Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(), "No existe una Organizacion con ese Identificador",
                            Toast.LENGTH_SHORT).show();
            }
        });

        mboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Idorg.getText().toString();
                String nombre = Nombreorg.getText().toString();
                String email = Emailorg.getText().toString();
                String telefono = Telefonorg.getText().toString();
                ContentValues modificar = new ContentValues();
                modificar.put("nombreorgs", nombre);
                modificar.put("emailorgs", email);
                modificar.put("telefonorgs",telefono );
                int actualizar = database.update("orgs", modificar, "idorgs=" + id, null);
                database.close();
                Idorg.setText("");
                Nombreorg.setText("");
                Emailorg.setText("");
                Telefonorg.setText("");
                if (actualizar == 1)
                    Toast.makeText(getActivity(), "Se modificaron los datos de la persona", Toast.LENGTH_SHORT)
                            .show();
                else
                    Toast.makeText(getActivity(), "No existe una persona con este documento",
                            Toast.LENGTH_SHORT).show();
            }
        });
        // Inflate the layout for this fragment
        return rootView;
    }
}