package com.navegacioncajon.navegacioncajon;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by zeroc on 6/11/2016.
 */

public class BaseDeDatosSQLite extends SQLiteOpenHelper {

    public BaseDeDatosSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table personas(id integer primary key, nombre text, email text, telefono integer)");
        db.execSQL("create table orgs (idorgs integer primary key, nombreorgs text, emailorgs text, telefonorgs integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists personas"); //borra si existe
        db.execSQL("create table personas(id integer primary key, nombre text, email text, telefono integer)");
        db.execSQL("drop table if exists orgs"); //borra si existe
        db.execSQL("create table orgs (idorgs integer primary key, nombreorgs text, emailorgs text, telefonorgs integer)");

    }
}
