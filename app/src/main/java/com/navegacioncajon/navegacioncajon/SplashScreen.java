package com.navegacioncajon.navegacioncajon;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class SplashScreen extends Activity {

    VideoView vidHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash_screen);
        vidHolder= (VideoView) findViewById(R.id.videoSH);
        super.onCreate(savedInstanceState);



        try
        {
            vidHolder = new VideoView(this);
            setContentView(vidHolder);
            String path = "android.resource://" + getPackageName() + "/" + R.raw.splashvid;
            Uri video=Uri.parse(path);
            vidHolder.setVideoURI(video);
            vidHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {
                public void onCompletion(MediaPlayer mp) {
                    jump();
                }});
            vidHolder.start();

        } catch(Exception ex) {
            jump();
        }



    }

    private void jump()
    {
        if(isFinishing())
            return;
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


}
