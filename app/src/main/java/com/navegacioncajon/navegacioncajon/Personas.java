package com.navegacioncajon.navegacioncajon;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Marce on 24/7/2016.
 */
public class Personas extends Fragment {
    private EditText Id,Nombre, Email,Telefono;

    @Override
    //metodo para crear la vista del layout del fragmento
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.persona_layout,container,false);
        Button sboton = (Button)rootView.findViewById(R.id.BTNSAVE);
        Button cboton = (Button)rootView.findViewById(R.id.BTNCONS);
        Button dboton = (Button)rootView.findViewById(R.id.BTNDEL);
        Button mboton = (Button)rootView.findViewById(R.id.BTNMOD);
        sboton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Id=(EditText)rootView.findViewById(R.id.IDENT);
                Nombre=(EditText)rootView.findViewById(R.id.FULL_NAME);
                Email=(EditText)rootView.findViewById(R.id.ELEC_MAIL);
                Telefono=(EditText)rootView.findViewById(R.id.FONO);
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),"administracion",null,1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Id.getText().toString();
                String nombre = Nombre.getText().toString();
                String email = Email.getText().toString();
                String telefono = Telefono.getText().toString();
                ContentValues registrar = new ContentValues();
                registrar.put("id",id);
                registrar.put("nombre",nombre);
                registrar.put("email",email);
                registrar.put("telefono",telefono);
                database.insert("personas",null,registrar); //registramos en la BBDD
                database.close();//cerramos la conexion
                // Limpiamos Los Campos
                Id.setText("");
                Nombre.setText("");
                Email.setText("");
                Telefono.setText("");
                Toast.makeText(getActivity(),"Se Registraton Los datos correctamente",Toast.LENGTH_LONG).show();
            }
        });

        cboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Id.getText().toString();
                Cursor fila = database.rawQuery(
                        "select nombre,email,telefono  from personas where id=" + id
                                + "", null);
                if (fila.moveToFirst()) {
                    Nombre.setText(fila.getString(0));
                    Email.setText(fila.getString(1));
                    Telefono.setText(fila.getString(2));
                } else
                    Toast.makeText(getActivity(), "No existe una persona con este Id",
                            Toast.LENGTH_SHORT).show();
                database.close();


            }
        });

        dboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Id.getText().toString();
                int eliminar = database.delete("personas", "id=" + id + "", null);
                database.close();
                Id.setText("");
                Nombre.setText("");
                Email.setText("");
                Telefono.setText("");
                if (eliminar == 1)
                    Toast.makeText(getActivity(), "Se Elimino la persona Correctamente",
                            Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity(), "No existe una persona con dicho documento",
                            Toast.LENGTH_SHORT).show();
            }
        });

        mboton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                BaseDeDatosSQLite admin = new BaseDeDatosSQLite(getActivity(),
                        "administracion", null, 1);
                SQLiteDatabase database = admin.getWritableDatabase();
                String id = Id.getText().toString();
                String nombre = Nombre.getText().toString();
                String email = Email.getText().toString();
                String telefono = Telefono.getText().toString();
                ContentValues modificar = new ContentValues();
                modificar.put("nombre", nombre);
                modificar.put("email", email);
                modificar.put("telefono",telefono );
                int actualizar = database.update("personas", modificar, "id=" + id, null);
                database.close();
                Id.setText("");
                Nombre.setText("");
                Email.setText("");
                Telefono.setText("");
                if (actualizar == 1)
                    Toast.makeText(getActivity(), "Se modificaron los datos de la persona", Toast.LENGTH_SHORT)
                            .show();
                else
                    Toast.makeText(getActivity(), "No existe una persona con este documento",
                            Toast.LENGTH_SHORT).show();
            }
        });
        // Inflate the layout for this fragment
        return rootView;
    }
}