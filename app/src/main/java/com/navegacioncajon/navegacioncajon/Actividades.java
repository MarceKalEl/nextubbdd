package com.navegacioncajon.navegacioncajon;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Marce on 24/7/2016.
 */
public class Actividades extends Fragment {
    @Override
    //metodo para crear la vista del layout del fragmento
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.actividad_layout, container, false);
    }
}